import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';

import {HttpModule} from '@angular/http';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import {GroupSubjectPage} from "../pages/group-subject/group-subject";
import { StatusBar } from '@ionic-native/status-bar';
import { LongPressModule } from 'ionic-long-press';
import {LoginPage} from "../pages/login/login";
//import {UpdradePaymentPage} from "../pages/updrade-payment/updrade-payment";
import { MyApp } from './app.component';

import { HomePage } from '../pages/home/home';
import {SignUpPage} from '../pages/sign-up/sign-up'
import {ProfilePage} from '../pages/profile/profile';

import {TermsConditionPage} from "../pages/terms-condition/terms-condition"
import {TabsPage} from "../pages/tabs/tabs";
import {ForgotPasswordPage} from "../pages/forgot-password/forgot-password";
// import {TransectionPage} from '../pages/transection/transection';
import { IonicRatingModule } from 'ionic-rating';
import * as firebase from 'firebase';
import {WelcomePage} from "../pages/welcome/welcome";
import {SelectAccountPage} from "../pages/select-account/select-account";
import { AngularFirestore } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFireAuth } from 'angularfire2/auth';
import { AngularFireModule } from 'angularfire2'
import {RestProvider} from "../providers/rest/rest";
import {Network} from "@ionic-native/network";
import { Facebook } from '@ionic-native/facebook';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { File } from '@ionic-native/file';

import { FilePath } from '@ionic-native/file-path';
import {MainHomePage} from "../pages/main-home/main-home";
import {EditProfilePage} from "../../src/pages/edit-profile/edit-profile";

import {ChangepassPage} from "../pages/changepass/changepass";

import {AddsubjectPage} from "../pages/addsubject/addsubject";

import { Calendar } from '@ionic-native/calendar';
import { DatePipe } from '@angular/common';
import { GooglePlus } from '@ionic-native/google-plus';

import { NoInternetPage } from '../pages/no-internet/no-internet';
import { Interceptor } from '../providers/intercepter/interceptor';

import {SettingPage} from "../pages/setting/setting";

import { IonicSelectableModule } from 'ionic-selectable';

import {PrivacyPage} from "../pages/privacy/privacy";
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { IntroPage } from '../pages/intro/intro';
import { DataService } from '../providers/data-service';
 import { OneSignal } from '@ionic-native/onesignal';
//import { ChatFilterPip } from '../pip/chatfilterpip';
//  const config = {
//   apiKey: "AIzaSyC8m90riReYIJV8k0cI2n6UNKkM7Sv2c78",
//   authDomain: "gemtutor-a4d90.firebaseapp.com",
//   databaseURL: "https://gemtutor-a4d90.firebaseio.com",
//   projectId: "gemtutor-a4d90",
//   storageBucket: "gemtutor-a4d90.appspot.com",
//   messagingSenderId: "983002567097"
// }; 

 var config = {
  apiKey: "AIzaSyCqzn5bskg3YsmBCZfMU488CvNcAE7aDcQ",
  authDomain: "gemacademy-1fc5f.firebaseapp.com",
  databaseURL: "https://gemacademy-1fc5f.firebaseio.com",
  projectId: "gemacademy-1fc5f",
  storageBucket: "gemacademy-1fc5f.appspot.com",
  messagingSenderId: "871193662003"
}; 
firebase.initializeApp(config);
@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SignUpPage,  

    TabsPage,
  
    
    ProfilePage,
    
    EditProfilePage,
    SettingPage,
    WelcomePage,
  
    GroupSubjectPage,
    ForgotPasswordPage,
    MainHomePage,
    SelectAccountPage,

    TermsConditionPage,
   
    ChangepassPage,

    AddsubjectPage,
   
    NoInternetPage,
   
    PrivacyPage,
    IntroPage
  ],
  imports: [
    LongPressModule,
    BrowserModule,
    IonicSelectableModule,
    IonicRatingModule,
    AngularFireModule.initializeApp(config),
    HttpClientModule,   
    IonicModule.forRoot(MyApp, {scrollAssist: false, autoFocusAssist: false}),
    //ChatFilterPip
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage,
    MainHomePage,
    ProfilePage,
    GroupSubjectPage,
    
    SettingPage,
  
    TabsPage,

    SignUpPage,    
    TermsConditionPage,
    WelcomePage, 
  
    EditProfilePage,
    ForgotPasswordPage,   
    SelectAccountPage,
 
    ChangepassPage,
    
    AddsubjectPage,
   
    NoInternetPage,
 
    PrivacyPage,
    IntroPage
  ],
  providers: [
    { provide : HTTP_INTERCEPTORS, useClass: Interceptor, multi : true},
    StatusBar,
    RestProvider,
    Network,
    //Stripe,
    AngularFireAuthModule,
    AngularFirestore,
    AngularFireAuth,
    SplashScreen,
    Facebook,
    Camera,
    InAppBrowser,
    Crop,
    File,
    FileTransfer,
    FilePath,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Calendar,
    DatePipe,
    GooglePlus,
    DataService,
    OneSignal
  
  ]
})
export class AppModule {
  
}
