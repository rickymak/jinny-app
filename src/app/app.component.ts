import {Component, ViewChild} from '@angular/core';
import {Platform, Events, NavController, App, AlertController} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {Network} from '@ionic-native/network';
import {LoginPage} from "../pages/login/login";
import {HomePage} from "../pages/home/home";
import {RestProvider} from "../providers/rest/rest";
import {NoInternetPage} from '../pages/no-internet/no-internet';
import {el} from "@angular/platform-browser/testing/src/browser_util";

// import { OneSignal } from '@ionic-native/onesignal';

import { IntroPage } from '../pages/intro/intro';
import {MainHomePage}  from '../pages/main-home/main-home'
import { from } from 'rxjs/observable/from';

declare var window;

export interface MenuItem {
  title: string;
  component: any;
  icon: string;
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild('nav') nav: NavController;
  rootPage: any;
  UserName: any;
  Email: any;
  appMenuItems: Array<MenuItem>;
  pages: Array<{ title: string, component: any, icon: string }>;
  lastActivePage;

  constructor( public alertCtrl: AlertController, public network: Network, public app: App, public rest: RestProvider, public events: Events, public platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
   this.userlogin();
    // if (localStorage.getItem('id')) {
    //   this.rest.checkUserStatus().then((result) => {
    //     if (result['status'] == true && result['response_code'] == 'SUCCESS') {
    //       this.userlogin();
    //     } else if (result['status'] == false && result['response_code'] == 'BLOCKED_USER') {
    //       this.app.getRootNav().setRoot(LoginPage, {'message': result['message']});
    //     } else if (result['status'] == false && result['response_code'] == 'USER_NOT_FOUND') {
    //       this.app.getRootNav().setRoot(LoginPage, {'message': result['message']});
    //     }
    //   }, err => {
    //   });
    // } else {
    //   this.userlogin();
    // }
    this.rest.setUpStringDictionary().then((res) => {
      this.rest.Lang = res;
    }, (err) => {
      console.log("this is err", err);
    });
    events.subscribe('user:login', () => {
      this.loggedIn();
    });
    let that = this;
    this.events.subscribe('connectionStatus', function (val) {
      if (!val.connected) {
        that.rootPage = NoInternetPage;
        return;
      } else {
        that.userlogin();
      }
    });
  
 // this.initPushNotification();
  }

  initializeApp() {
    this.platform.ready().then(() => {
    });
    this.platform.registerBackButtonAction(() => {
      let activeView = this.nav.getActive();
      if (activeView.name != 'CreatetimetablePage') {
        this.nav.pop();
      }
    });
    
  }

  // initPushNotification(){
  //   this.oneSignal.startInit('a76a891b-a802-45a0-9fb8-ca50e7bfa662', '871193662003');
  //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);
  //   this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
  //   this.oneSignal.handleNotificationReceived().subscribe((data) => {
  //     console.log("notification received");
  //     console.log(data);     
  //   });

  //   this.oneSignal.handleNotificationOpened().subscribe((data) => {     
  //     if(data['notification']['payload']['title'] == 'New request received'){
  //       this.nav.push(NotificationPage);
  //     }
  //     else{
      
  //       setTimeout(() => {
  //         this.nav.push(MessagePage);
  //       }, 3000);
  //     }
  //     console.log(data);
  //   });
    
  //   // this.oneSignal.getIds().then(ids => {
  //   //   console.log(ids);
  //   // });
  //   this.oneSignal.endInit();
  //   //this.MakeAnewNotification();
  // }
  userlogin() {
    const User = JSON.parse(localStorage.getItem("User")); 
  
    if (User != null) {
      this.rootPage = HomePage;
    } else if(User == null) {
      this.rootPage = IntroPage;
    }
  }

  loggedIn() {
    this.UserName = localStorage.getItem("Username");
    this.Email = localStorage.getItem("UserEmail");
  }
}

