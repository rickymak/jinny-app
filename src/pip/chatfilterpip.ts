import { Pipe, PipeTransform } from '@angular/core';


@Pipe({ name: 'chatFilterPip' })
export class ChatFilterPip implements PipeTransform {
  transform(allHeroes: any) {
    return allHeroes.filter(hero => hero.lasttime);
  }
}