import { Injectable } from '@angular/core';
import { HttpClient , HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class Interceptor implements HttpInterceptor{
  public timeout = null;
  public timer_is_on = 0;
  public sec = 0;
  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headersConfig = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
    };
    headersConfig['Authorization'] = "Basic YWRtaW46MTIzNDU=";
    headersConfig['X-API-KEY'] = "1234567";
    if(localStorage.getItem('id')){
      headersConfig['user_id'] = localStorage.getItem('id');
      headersConfig['user_type'] = localStorage.getItem('UserType'); 
    }
    // const request = req.clone({ setHeaders: headersConfig });
    const request = req.clone();
    let that = this;
    return next.handle(request);
  }
}
