import { HttpClient , HttpHeaders } from '@angular/common/http';
import { Injectable ,  } from '@angular/core';
import { Network } from '@ionic-native/network';
import { ToastController, Events} from 'ionic-angular';
import { Observable } from 'rxjs';
import {FileTransfer, FileTransferObject, FileUploadOptions} from "@ionic-native/file-transfer";
import { Http, Response } from "@angular/http";
import 'rxjs/add/operator/map';
import { parseTemplate } from 'ionic-angular/umd/util/datetime-util';

@Injectable()
export class RestProvider  {
  liveUrl = 'https://gemacademy.sg/'
  newUrl = 'http://52.42.2.170/gem/'; 
  Lang :any="";
 BaseUrl = 'http://3.134.218.36/';
  //BaseUrl ='http://localhost:3001/'
 // BaseUrl = this.liveUrl+'api/';
  imageUrl=this.BaseUrl+'img/Image';
  uploadImageUrl=this.liveUrl+'api/upload/uploadIMG';
  addImageUrl=this.liveUrl+'public/uploads/advertisement/';
  GetImage = this.liveUrl+'public/';
  networkCheck:boolean=true;
  constructor(public toastCtrl:ToastController,public http: HttpClient ,private transfer: FileTransfer,public network:Network, private events :Events) {
    this.Lang;
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.networkCheck=false;
      this.checkNetwork();
      this.events.publish("connectionStatus", {connected : false});
    });

    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');
      this.networkCheck=true;
      this.checkNetwork();
      this.events.publish("connectionStatus", {connected : true});
    });
  }
 
  checkNetwork(){
    console.log("network check");
    let msg="";
    if(this.networkCheck == true){
      msg="Network Connected.";
    }else{
      msg="Please check your network connection.";
    }
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.present();
  }
  Registration(data,kk)
  {
    // let url  = this.BaseUrl+ (type==1 ? "tutor/signup" : "student/signup");
    return new Promise((resolve, reject) => {       
      // this.http.post(url,data,{})
      this.http.post(this.BaseUrl + "reg/registration", data)
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  } 
  gettutortutee(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"tutor/getTutorStudent",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  loginCheck(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"tutor/loginsocial",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  uIdUpadate(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"tutor/update_uId",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  getsubjectlist(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"common/getUserSubjects",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  login(data)
  {    //alert(JSON.stringify(data)+"Data Send From Api");

    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"reg/login", data )
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  level()
  {    //alert(JSON.stringify(data)+"Data Send From Api");
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"tutor/GetLevel")
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  banner()
  {    //alert(JSON.stringify(data)+"Data Send From Api");
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"tutor/getAdvertisement")
      .timeout(5000)
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  getCountry()
  {    //alert(JSON.stringify(data)+"Data Send From Api");
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"tutor/getCountry")
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  levelsub(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "tutor/getSubjectCategory", data, {})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  featuredTutor(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "tutor/getFeaturedTutor", data, {})
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  createtable(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "tutor/createSubject", data, {})
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  gettable(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "tutor/GetAvailability", data, {})
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  checkAcceptStatus(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"OfferRequest/AcceptRequest",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  checkRejectStatus(data)
  {
    return new Promise((resolve, reject) => {       
      this.http.post(this.BaseUrl+"OfferRequest/RejectRequest",data,{})
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  getTableByUser(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "common/getUserTimetable", data, {})
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  selectSubject(data) {
    return new Promise((resolve, reject) => {
      let url = this.BaseUrl + (data.type==1 ? "tutor/GetsubjectId" : "student/GetsubjectId");

      this.http.post(url, JSON.stringify(data), {})
      
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  reverseTime(data) {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/blockAvailabilityTime",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  ratingChat(data)
  {
    return new Promise((resolve, reject) => {
      let url = this.BaseUrl + (data.type==1 ? "tutor/getTutorAcceptedSubject" : "student/getStudentAcceptedSubject");
      this.http.post(url, JSON.stringify(data), {})
        .subscribe(res => {


          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  EditSubject(data) {
    return new Promise((resolve, reject) => {
      let url = this.BaseUrl + (data.type==1 ? "tutor/addSubject" : "student/addSubject");
      this.http.post(url, JSON.stringify(data), {})
        .subscribe(res => {
          // console.log(res['_body']); Subjectdelete
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  deleteSubject(data) {
    return new Promise((resolve, reject) => {
    
      let url = this.BaseUrl + (data.type==1 ? "tutor/deleteTutorStudentSubject" : "student/addSubject");

      this.http.post(url, JSON.stringify(data), {})
      
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  subjectselect(data, type) {
    return new Promise((resolve, reject) => {
    
      let url = this.BaseUrl + (type==1 ? "tutor/GetTutorSubjByID" : "student/GetStudentSubj");
      this.http.post(url, JSON.stringify(data), {})
      
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  datewisesubject(data) {
    return new Promise((resolve, reject) => {    
      this.http.post(this.BaseUrl+"common/getAvalabilityByDay",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  Subjectdelete(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/deleteTutorStudentSubject",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
}
checktimetable(data)
{
  return new Promise((resolve, reject) => {
    this.http.post(this.BaseUrl+"tutor/checkSubjectAvailability",data)
      .subscribe(res => {
        // console.log(res['_body']);
        resolve(res);
      }, (err) => {
        reject(err);
      });
  });
}
    fetchTutorData(data)
    {
      return new Promise((resolve, reject) => {
        this.http.post(this.BaseUrl+"tutor/getUserDetailById",data)
          .subscribe(res => {
            // console.log(res['_body']);
            resolve(res);
          }, (err) => {
            reject(err);
          });
      });
    }
  fetchTutorProfile(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/user",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  fetchTuteeProfile(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"student/user",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  GetSubject1(data)
  { 
    return new Promise((resolve, reject) => {     
      this.http.post(this.BaseUrl+"tutor/GetSubject",data,{})
        .subscribe(res => {

         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  levelsearch(data)
  { 
    return new Promise((resolve, reject) => {     
      this.http.post(this.BaseUrl+"tutor/GetLevel",data,{})
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  searchtutee(data)
  { 
    return new Promise((resolve, reject) => {     
      this.http.post(this.BaseUrl+"tutor/GetSubject",data,{})
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  checkavail(data)
  { 
    return new Promise((resolve, reject) => {     
      this.http.post(this.BaseUrl+"tutor/GetAvailability",data,{})
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  Subject(data)
  { 
    return new Promise((resolve, reject) => {     
      let url = this.BaseUrl + (data.type==1 ? "tutor/addSubject" : "student/addSubject");

    
      this.http.post(url, JSON.stringify(data), {})
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  passwordget(data)
  { 
    return new Promise((resolve, reject) => {     
      this.http.post(this.BaseUrl+"reg/getPassword",data,{})
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  userLogingoogle(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"student/signup",data,{})      
        .subscribe(res => {       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         
          reject(err);
        });
    });
  }
  dofav(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"favourite/StudentTutorFavourite",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  subjectlistStudent(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"student/GetStudentSubj",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  subjectlistTutor(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/GetTutorSubjByID",data,{})
      .timeout(5000)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

 

  AboutUsAllxount( )
  {

    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"common/getAllCount")
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  Fav(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"favourite/getStudentTutorFavourite",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  his(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/getHistory",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  FavStatus(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"favourite/getFavourite",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  Filter(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"search/find",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
/*  requestsubject(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/getTutorStudentSubject",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }*/
  requestsubject(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/getTutorStudentSubject",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  requestsent(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/insertSubjRequest",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  addacademic(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/addAcademic",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  academiclist(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/getAcademicByUser",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  deleteAcademic(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/deleteAcademic",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  requestbyid(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/getSendRequestById",data )
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  ReceiveRequestById(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/getReceiveRequestById",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  AllAccept(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/acceptRequest",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  AllReject(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/rejectRequest",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  requestcount(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/getRequestCount",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getrequestbyid(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/getSendReqUserdata",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  singlereject(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/rejectRequest",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  singleaccept(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/acceptRequest",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getnotification(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/getRequestCount",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  notificationlist(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/getNewReqData",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  changepassword(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"reg/ChangePassword",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  createevent(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/createEvent",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getSubject()
  {
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"common/getAllSubjects")
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getLocation()
  {
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"common/getCountry")
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  proof(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/verifyUser",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  proofCheck(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/isVerified",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  account_details(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/upgradePlan",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  bump_Paydetails(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/bumpPost",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  get_feture(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/isUserFeatured",data,{})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  rating(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/rating",data,{})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getrating(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"tutor/is_rating_exists",data,{})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getratingStatus(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/getRequestStatus",data,{})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  saveratingStatus(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/rating",data,{})
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  setUpStringDictionary(){
    return new Promise((resolve, reject)=>{
      this.http.get('assets/data.json')
      .subscribe(res => {     
        console.log("this is res", res);
        resolve(res);
      }, (err)=>{
        reject(err);
      });
    });
  }
  checkUserStatus(){
    return new Promise((resolve, reject)=>{   
      let postParams = {
        user_id: localStorage.getItem('id'),
        user_type: localStorage.getItem('UserType'),
      }  
      const url = this.BaseUrl+"common/CheckBlockedUser"+'?user_id='+postParams.user_id+'&user_type='+postParams.user_type;
      //  this.http.get(this.BaseUrl+"common/CheckBlockedUser") 
      this.http.get(url) 
        .subscribe(res => {
          console.log("this is res", res);
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }
  checkVerifyAmnt(){
    return new Promise((resolve, reject)=>{
      this.http.get(this.BaseUrl+'common/getPaymentAmount')
        .subscribe(res => {
          console.log("this is res", res);
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }

  getBumpPlan(){
  return new Promise((resolve, reject)=>{
  this.http.get(this.BaseUrl+'common/getSettingData')
  .subscribe(res => {
    console.log("this is res", res);
    resolve(res);
   }, (err)=>{
     reject(err);
    });
   });
  }

    deleteNotification(data)
    {
      return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"request/DeleteNotification",data,{})
      .subscribe(res => {
      resolve(res);
      }, (err) => {
        reject(err);
     });
   });
}
  getCity(){
    return new Promise((resolve, reject)=>{
      this.http.get(this.BaseUrl+'common/GetCity')
        .subscribe(res => {
          console.log("this is res", res);
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }
  getSub(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"search/find",data,{})
        .subscribe(res => {

          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  subjectUp1(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/setUserSubjectOrder",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  subjectDown(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/setUserSubjectOrder",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getAdd(){
    return new Promise((resolve, reject)=>{
      this.http.get(this.BaseUrl+'common/getYear')
        .subscribe(res => {
          console.log("this is res", res);
          resolve(res);
        }, (err)=>{
          reject(err);
        });
    });
  }
  logOut(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"common/logout",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  terms()
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"student/getTermCondition",{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  Policy()
  {    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"student/getPrivacyPolicy",{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  // getMenus(){
  //   return this.http.get('assets/data/menus.json')
  //    .map((response:Response)=>response.json());
  // }
  getMenus(){
    return new Promise((resolve, reject) => {
      this.http.get('assets/data/menus.json')
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
    });
  };
  allsubjectcategory(){
    return new Promise((resolve, reject) => {
   //   this.http.post(this.BaseUrl+"common/GetAllCatSubcatSubject",{})
      this.http.get(this.BaseUrl+"common/GetAllCatSubcatSubject")
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
    });
  };

  ////////     Jinny app List

  MainCategory()
  {    alert("Data Send From Api");
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"Main/all")
        .subscribe(res => {
          resolve(res);
        }, (err) => {
         alert(err)
          reject(err);
        });
    });
  }

  City()
  {  
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"Count/city")
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         alert(err)
          reject(err);
        });
    });
  }
  getMainCat()
  {  
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"Main/all")
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         alert(err)
          reject(err);
        });
    });
  }

  uploadImg()
  {  
    return new Promise((resolve, reject) => {
      this.http.get(this.BaseUrl+"Main/all")
        .subscribe(res => {
       
         // console.log(res['_body']);
          resolve(res);
        }, (err) => {
         alert(err)
          reject(err);
        });
    });
  }

  subCatById(data) {    
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "img/findById", data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  GetPriceId(data) {    
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl + "Price/findById", data)
        .subscribe(res => {
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  Query(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"Qry/save",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }
  getBillDetails(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"Qry/findBill",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }
  Contactus(data)
  {
    console.log(JSON.stringify(data)+"Data Send From Google Api");
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"contact/save",data,{})
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {

          reject(err);
        });
    });
  }

  fetchUserData(data)
  {
    return new Promise((resolve, reject) => {
      this.http.post(this.BaseUrl+"reg/getUserVendor",data)
        .subscribe(res => {
          // console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  }

  profileUpdate(data)
  {
    // let url  = this.BaseUrl+ (type==1 ? "tutor/signup" : "student/signup");
    return new Promise((resolve, reject) => {       
      // this.http.post(url,data,{})
      this.http.post(this.BaseUrl + "reg/ProfileUpate", data)
        .subscribe(res => {
          console.log(res['_body']);
          resolve(res);
        }, (err) => {
          reject(err);
        });
    });
  } 
}




