import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';

@Injectable()
export class DataService {

  constructor(public http: HttpClient) {
    console.log('Hello DataService Provider');
  }

  getMenus(){
    return new Promise((resolve, reject) => {
      this.http.get('assets/data/menus.json')
      .subscribe(res => {
        resolve(res);
      }, (err) => {
        reject(err);
      });
    });
  };
}
