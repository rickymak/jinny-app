import { Component } from '@angular/core';
import {
  AlertController,
  App,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import {Http} from "@angular/http";
import {HomePage} from "../home/home";
import {RestProvider} from "../../providers/rest/rest";


/**
 * Generated class for the ContactUsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {

  Name:any;
  Email:any;
  Mobile:any;
  Message:any;
  Address:any;
  public lang;
  constructor(public loadingCtrl: LoadingController ,private toastCtrl: ToastController,public rest:RestProvider,public app :App,public alertCtrl: AlertController,public navCtrl: NavController, public navParams: NavParams  ) {
    this.lang=this.rest.Lang;
    this.Name='';
    this.Email ='';
    this.Mobile='';
    this.Address='';
    this.Message='';
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ContactUsPage');
  }
  Getintouch(myForm){
    if(this.rest.networkCheck == true){
      if(myForm.status === 'INVALID')
      {
        Object.keys(myForm.controls).forEach(field => {
          const control = myForm.controls[field];
          control.markAsTouched({ onlySelf: true });
        })
      }
      else{

        let loader = this.loadingCtrl.create({
          content: "loading..."
        });
        loader.present();
        let postParams = {
          fullName:this.Name,
          email:this.Email,
          phoneNumber:this.Mobile,
          message:this.Message,
          address:this.Address
          
        }
        console.log(postParams);
        this.rest.Contactus( (postParams))
          .then(data =>
          {
            if(data['status']==true){
              const toast = this.toastCtrl.create({
                message:data['message'],
                duration: 3000
              });
              toast.present();
              loader.dismiss();
              this.navCtrl.pop();
            }
           
            }, err => {
            if(err.status == 405 || err.status == 500 || err.status == 0){
              const toast = this.toastCtrl.create({
                message: 'Internal Serrver Error'+'-'+err['status'],
                duration: 3000
              });
              toast.present();
            }
            loader.dismiss();
            console.log(err);// Error getting the data
          });
      }
    }
    else{
      this.rest.checkNetwork();
    }



  }
  clear(){
    this.Name='';
    this.Email ='';
    this.Mobile='';
    this.Message='';
  }
}
