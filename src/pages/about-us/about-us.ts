import { Component } from '@angular/core';
import {
  AlertController,
  App,
  IonicPage,
  LoadingController,
  NavController,
  NavParams,
  ToastController
} from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";

@IonicPage()
@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
})
export class AboutUsPage {
  user: any;
  full_name: any;
  notification: any;
  loader: any;
  UserData: any;
  checkcount: any;
  CountData: any;
  constructor(public loadingCtrl: LoadingController, public rest: RestProvider, public app: App, public alertCtrl: AlertController, public navCtrl: NavController, public toastCtrl: ToastController) {
    this.checkusertype();
    this.loader = this.loadingCtrl.create({
      content: "loading..."
    });
    this.loader.present();
    this.getcount();
  }

  ionViewWillEnter() {
    console.log('Runs when the page is about to enter and become the active page.');
  }
  checkusertype() {
    var user_typeid = localStorage.getItem("UserType");
    this.full_name = localStorage.getItem("Username");
    if (user_typeid == '2') {
      this.user = 'Learner';
    }
    else {
      this.user = 'Trainer';
    }
  }

  getcount() {
    this.rest.AboutUsAllxount()
      .then(data => {
        this.CountData = data['data'];
        console.log(this.CountData);

        this.loader.dismiss();
      }, error => {
        console.log(error);// Error getting the data
        this.loader.dismiss();
      });
  }
  pushtocontact() {
    
  }
}
