import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";

/**
 * Generated class for the PrivacyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-privacy',
  templateUrl: 'privacy.html',
})
export class PrivacyPage {
  message:any;
  constructor( public rest:RestProvider,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() { 
    this.rest.Policy().then((result)=>{
      this.message=result['message'];
    },err=>{
 
    })
   }

}
