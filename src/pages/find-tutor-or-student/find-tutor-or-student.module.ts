import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FindTutorOrStudentPage } from './find-tutor-or-student';

@NgModule({
  declarations: [
    FindTutorOrStudentPage,
  ],
  imports: [
    IonicPageModule.forChild(FindTutorOrStudentPage),
  ],
})
export class FindTutorOrStudentPageModule {}
