import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController,AlertController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { HomePage } from '../home/home';
import { RestProvider } from "../../providers/rest/rest";
import { IonicSelectableComponent } from 'ionic-selectable';
import {SignUpPage} from '../sign-up/sign-up'
@IonicPage()
@Component({
  selector: 'page-find-tutor-or-student',
  templateUrl: 'find-tutor-or-student.html',
})
export class FindTutorOrStudentPage {
  GetPriceList:any;
  select: any;
  relationship: any;  
  User
  public lang;
  Name:any
  Email;any
  Mobile:any
  Address:any
  State:any;
  address:any;
  Message:any;
  ServiceTime:any;
  constructor(public alertCtrl: AlertController,private toastCtrl: ToastController, public rest: RestProvider, public loadingCtrl: LoadingController, public navCtrl: NavController, public navParams: NavParams) {
    this.CityList()
    this.User = JSON.parse(localStorage.getItem("User"));
    this.lang=this.rest.Lang;
    this.relationship = "schedule";
    this.PriceList();
   this.getUserDetails();
  // this.CityList()
  }


getUserDetails(){

  if(this.User != null){
    this.Name = this.User.name;
    this.Email =this.User.email
    this.Mobile   = this.User.phone
    this.Address = this.User.address
  }
 
}
  date1(val) {
    var date = val;
    var spilt = date.split(" ");
    var getDay = spilt[0];
    var remove = getDay.split("-");
    return remove[2] + '-' + remove[1] + '-' + remove[0]
  }
  date2(val) {
    var date = val;
    var spilt = date.split(" ");
    var getDay = spilt[0];
    var remove = getDay.split("-");
    return remove[2] + '-' + remove[1] + '-' + remove[0]
  }

  PriceList()
  {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
      if(this.rest.networkCheck == true){
var id = localStorage.getItem("SubID");
          let postData={
            postId: id,
             
          };
          this.rest.GetPriceId((postData)).then((result)=>{
              if(result['status'] == true ){
                loader.dismiss();
                  this.GetPriceList = (result["data"]);
                 
              }
              else if(result['status'] == false)  {
                loader.dismiss();
               //   this.validation='NotFound';
              }
          }, (err) => {
            
              if(err.status == 405 || err.status == 500 || err.status == 0){
                  const toast = this.toastCtrl.create({
                     // message: err['status'],
                      message: 'Internal Serrver Error'+'-'+err['status'],
                      duration: 3000
                    });
                    toast.present();
                    loader.dismiss();
              }                
          });
      }
      else{
          this.rest.checkNetwork();
      }
  }

  Getintouch(myForm){
    if(this.User != null){
    if(this.rest.networkCheck == true){
      if(myForm.status === 'INVALID')
      {
        Object.keys(myForm.controls).forEach(field => {
          const control = myForm.controls[field];
          control.markAsTouched({ onlySelf: true });
        })
      }
      else{

        let loader = this.loadingCtrl.create({
          content: "loading..."
        });
       // loader.present();
        let postParams = {
          id:this.User._id,
          name:this.Name,
          email:this.Email,
          phone:this.Mobile,
          address:this.Address,
          MainCategoryId:localStorage.getItem('MainCategoryId'),
          SubCategoryId:localStorage.getItem('SubID'),
          Message:this.Message,
          ServiceTime:this.ServiceTime,
          Status:'Open'
        }
        console.log(postParams);
       this.rest.Query( (postParams))
          .then(data =>
          {
            console.log(data)
            if(data['status']){
              let alert = this.alertCtrl.create({
                title: 'Message',
                message: data['message'],
                cssClass: 'arrow-account',
                buttons: [
                    {
                        text: 'Ok',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                            this.navCtrl.setRoot(HomePage);
                        }
                    }

                ]
            });
            alert.present();
       
            }
            if(data['status'] == false && data['response_code']=='MESSAGE_NOT_SENT'){
              loader.dismiss();
              alert(data['message']);
              const toast = this.toastCtrl.create({
                message: 'Internal Serrver Error',
                duration: 3000
              });
              toast.present();
            }
            }, err => {
            if(err.status == 405 || err.status == 500 || err.status == 0){
              const toast = this.toastCtrl.create({
                message: 'Internal Serrver Error'+'-'+err['status'],
                duration: 3000
              });
              toast.present();
            }
            loader.dismiss();
            console.log(err);// Error getting the data
          });
      }
    }
    else{
      this.rest.checkNetwork();
    }
  }
  else{
    let alert = this.alertCtrl.create({
      title: 'Message',
      message: 'Please Login to sent a query',
      cssClass: 'arrow-account',
      buttons: [
        {
          text: 'Ok',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');        
            this.navCtrl.setRoot(SignUpPage);
          }
        }
  
      ]
    });
    alert.present();
   } 


  }
  clear(){
    this.Name='';
    this.Email ='';
    this.Mobile='';
    this.Address='';
  }

  portChange(event: {
    component: IonicSelectableComponent,
    value: any
}) {
   console.log('port:', event.value);
  //   this.SelectState = event.value['districts']
  //  // console.log('this.SelectState>>',this.SelectState.length)
  //   for (let i = 0; i < this.SelectState.length; i++) {
  //       this.requestData = {             
  //         "Name": this.SelectState[i]
  //       } 
  //       this.CityData.push(this.requestData);
  //     //  console.log('Get this.CityData',this.CityData)           
  //     }

}
CityList() {   
  // this.spinnerService.show()
   this.rest.City().then(
     (result: any) => {
      this.State = result['City']['states']
      console.log('state>>>>>',this.State)  
      //  if (result.status &&  result.ResponeCode == 'Successful') {
      //    this.State = result.data          
      //  }
      //  else {
      //    alert("data not found")
      //  }
     
     }, (err) => {
 
       if (err.status == 405 || err.status == 500 || err.status == 0) {    
       }
 
     }
   );
 }
}
