import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams , Platform } from 'ionic-angular';
import {LoginPage} from "../login/login";
import { Slides } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/**
 * Generated class for the IntroPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-intro',
  templateUrl: 'intro.html',
})

export class IntroPage {
  @ViewChild('slides') slides: Slides;
  private index:any = 0;
  constructor(private splashScreen: SplashScreen,public navCtrl: NavController, public navParams: NavParams,private platform: Platform,private statusBar: StatusBar,) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#13364f');
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.index=0;
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad IntroPage');
  }
  goToHome(){
    this.navCtrl.setRoot(LoginPage);
  }
  next() {
    this.goToHome()
    // this.index = this.index == 2 ? this.goToHome() : this.index+1;
    // this.slides.slideTo(this.index)
  }
  prev() {
    this.slides.slidePrev();
    this.index = this.index==0 ? 0 : this.index-1;
  }
}
