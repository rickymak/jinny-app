import { Component, ViewChild } from '@angular/core';
import {
  Nav,
  NavController,
  Platform,
  LoadingController,
  App,
  AlertController,
  NavParams,
  Tabs,
  Events,
  MenuController
} from 'ionic-angular';
import { TabsPage } from "../tabs/tabs";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { RestProvider } from "../../providers/rest/rest"
import { AddsubjectPage } from "../addsubject/addsubject";
import { LoginPage } from "../login/login";
import { SettingPage } from "../setting/setting";
import { AngularFireAuth } from 'angularfire2/auth';

export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
  icon: string;
}
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('mainTabs') tabRef: Tabs;

  // Basic root for our content view
  rootPage: any;
  GetLevel: any;
  UserName: any;
  image: any;
  userType: boolean;
  Email: any;
  url: any;
  UserLogoin: any;
  imageUrl
  // Reference to the app's root nav
  @ViewChild(Nav) nav: Nav;

  pages: any = [
    { title: 'Home', pageName: 'HomePage', icon: 'ios-home-outline' },
    // { title: 'Bill Status', pageName: AddsubjectPage, icon: 'ios-book-outline' },
    // { title: 'Bill Status', icon: 'ios-copy-outline' },
    // { title: 'My Offer',  icon: 'ios-person-add-outline' },
    { title: 'About Us', pageName: 'AboutUsPage', icon: 'ios-clipboard-outline' },
    { title: 'Contact Us', pageName: 'ContactUsPage', icon: 'ios-contact-outline' },

  ];
 
  constructor(private event: Events, private navParam: NavParams, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, public alertCtrl: AlertController, public app: App, public menuCtrl: MenuController, public loadingCtrl: LoadingController, public rest: RestProvider, public navCtrl: NavController, public afireauth: AngularFireAuth) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#13364f');
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.loggedIn();
    this.rootPage = TabsPage;
    this.url = this.rest.imageUrl;
    this.refreshProfile();
    this.UserName = localStorage.getItem("UserName");
    if (localStorage.getItem('status') == null) {
      this.userType = true;
    }
    else {
      this.userType = false;
    }
    if (localStorage.getItem('UserType') == '1') {
      this.UserLogoin = 'Trainer';
    }
    else {
      this.UserLogoin = 'Learner';
    }
    this.event.subscribe('reloadProfile', () => {
      this.refreshProfile();
    });
  }
  openPage(page: PageInterface) {
    let params = {};


    if (page.index) {
      params = { tabIndex: page.index };
    }


    if (this.nav.getActiveChildNav() && page.index != undefined) {
      this.nav.getActiveChildNav().select(page.index);
    } else {
      if (page.pageName == "HomePage") {
        this.nav.getActiveChildNav().select(0);
      } else
        this.nav.push(page.pageName, params);
    }

  }

  refreshProfile() {
    if (localStorage.getItem('image') == undefined) return;
    this.imageUrl = localStorage.getItem('image').includes('http') ? localStorage.getItem('image') : this.url + localStorage.getItem('image');
    this.image = (localStorage.getItem('image') != null) ? this.imageUrl : 'assets/imgs/user.png';
    this.UserName = localStorage.getItem("UserName");
  }

  isActive(page: PageInterface) {
    let childNav = this.nav.getActiveChildNav();

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'primary';
      }
      return;
    }
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'primary';
    }
    return;
  }

  firebaseLocalStoreClear() {
    this.afireauth.auth.signOut().then(function () {
    }).catch(function (error) {
    });
  }

  logout() {
    let alert = this.alertCtrl.create({
      title: 'Confirm Message',
      enableBackdropDismiss: false,
      message: 'Do you wish to Logout ?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Please wait...",
              duration: 1500
            });
            this.rest.logOut({ user_id: localStorage.getItem('id'), user_type: localStorage.getItem('UserType') })
            localStorage.clear();
            this.firebaseLocalStoreClear();
            this.app.getRootNav().setRoot(LoginPage);
            loader.present();
          }
        },
        {
          text: 'No',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }

  ionViewWillEnter() {
    if (this.navParam.get('fromDetail')) {
      this.nav.getActiveChildNav().select(3);
    }
  }

  loggedIn() {
    this.UserName = localStorage.getItem("Username");
    this.Email = localStorage.getItem("UserEmail");
  }
  setting() {
    this.navCtrl.push(SettingPage)
  }
  profile() {
    if (this.navCtrl.getActive().instance.rootPage.name == 'TabsPage') {
      this.menuCtrl.close()
      this.nav.getActiveChildNav().select(3);
    }
  }
}
