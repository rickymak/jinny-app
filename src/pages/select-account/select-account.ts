import { Component } from '@angular/core';
import {App, IonicPage, NavController, NavParams, ToastController,Platform} from 'ionic-angular';
import {SignUpPage} from "../sign-up/sign-up";
import {LoginPage} from "../login/login";
import { StatusBar } from '@ionic-native/status-bar';
/**
 * Generated class for the SelectAccountPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-account',
  templateUrl: 'select-account.html',
})
export class SelectAccountPage {
  NextPush:any='';
  color:any='';
  colors:any='';
  constructor(private platform: Platform,private statusBar: StatusBar,public navCtrl: NavController,public app : App, public toastCtrl: ToastController, public navParams: NavParams) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#13364f');
      statusBar.styleDefault();
     
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SelectAccountPage');
  }
  showErrorToast(data: any) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  Next() {
    if(this.NextPush ==''){
      this.showErrorToast('Please Select Partner or User');
    }
    else {
      this.navCtrl.push(SignUpPage);
    }
  }
  close(){
     this.navCtrl.pop();
  }
  Vendor(){
    this.color ="change";
    this.colors ="";
    this.NextPush ="TutorPush";
    localStorage.setItem('UserType','Partner');
  }
  User(){
    this.colors ="changes";
    this.color ="";
    this.NextPush ="TuteePush";
    localStorage.setItem('UserType','User');
  }
}
