import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams , App} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {SignUpPage} from "../sign-up/sign-up";
/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {

  constructor(public app : App ,public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OtpPage');
  }
  click(){
    this.app.getRootNav().setRoot(LoginPage);
  }
  back()
  {
    this.app.getRootNav().setRoot(SignUpPage);
  }
}
