import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ProfilePage} from "../profile/profile";
import {MainHomePage} from "../main-home/main-home";

import {AddsubjectPage} from "../addsubject/addsubject";
import * as firebase from 'firebase'
import { RestProvider } from '../../providers/rest/rest';
/**
 * Generated class for the TabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  tab1Root: any = MainHomePage;
 
  tab3Root: any = AddsubjectPage;
  tab4Root: any = ProfilePage;
  myIndex: number;
  unreadMessage = 0;
  firebuddychats = firebase.database().ref('/Gemchats');
  constructor(public navCtrl: NavController, public navParams: NavParams, private rest : RestProvider) {
    this.myIndex = navParams.data.tabIndex || 0;
    localStorage.setItem('tabIndex', ""+this.myIndex);
  }

  setSelectedIndex(index){
    localStorage.setItem('tabIndex', index);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  //  this.getAllMessages();
  }

  getAllMessages(){
    if(this.rest.networkCheck == true){ 
      var user_typeid = localStorage.getItem("UserType");
      let postParams = {
        user_type  : user_typeid=='1' ? '2' : '1'       
      }   
      this.rest.gettutortutee(JSON.stringify(postParams)).then((result)=>{
        if(result['status'] == true &&  result['response_code'] == 'SUCCESS'){
          this.getUnreadMessage(result["data"]);
        }
      });
  }}

  getUnreadMessage(UserData){
    let that = this;
    that.unreadMessage = 0;
    console.log("starting", that.unreadMessage);
    let fireBaseUser = localStorage.getItem("UserFireBaseID");
    UserData.forEach(function(objj){
      let buddy = objj.u_id;
      if(buddy=="") return;
      that.firebuddychats.child(fireBaseUser).child(buddy).on('value', (snapshot) => {
        let obj = snapshot.val();
        that.unreadMessage = 0;
        if(obj){
          Object.keys(obj).forEach(function(val){
            if(!obj[val].read){
              that.unreadMessage = that.unreadMessage+1;
              console.log("this is number", that.unreadMessage);
            }
          });
        }
      });
      console.log("end", that.unreadMessage);
    })
  }

}
