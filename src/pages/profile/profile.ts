import { Component } from '@angular/core';
import { IonicPage, NavController, Events, NavParams, LoadingController, ToastController, AlertController, ModalController } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";
import { el } from "@angular/platform-browser/testing/src/browser_util";

import { EditProfilePage } from "../edit-profile/edit-profile";


import { Calendar } from '@ionic-native/calendar';
import { DatePipe } from '@angular/common';


/**
 * Generated class for the ProfilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {  
  pet: any;
  favDataAvailable:boolean;
  addsubjecttest: boolean;
  contactnumber: any;
  check:any=[];
  name: any;
  enableRating:boolean;
  Show:any={};
  test:boolean=true;
  tutorId: any;
  from_time: any;
  bumped:boolean=false;
  valueSubject:any;
  pr: boolean = false;
  ownProfile: boolean = true;
  readonly: boolean = false;
  ownProfile1: boolean = true;
  location: any;
  to_time: any;
  image: any;
  age: any;
  selectedDate: any;
  address: any;
  sujectavailable: boolean;
  visibleIndex: any;
  title: string = 'My Profile';
  subjectSelected: any;
  count: any;
  noFav: boolean = true;
  favMsg:any;
  description: any;
  subjects: any = [];
  imageUrl: any;
  verified: boolean = false;
  days: any;
  type: any;
  other: any;
  biography: any;
  index: any;
  joinDate: any;
  verifiedmsg:boolean;
  rate: any;
  id: any;
  feature_quote: any;
  UserAcademic: any;
  deleteID: any;
  Upgrade: boolean = false;
  Usertype: any;
  pricePersession: any;
  UserloginID: any;
  UserSelectID: any = '';
  define: any;
  UserData: any;
  msg: any;
  UserData1: any;
  usertype: any;
  UserData2: any;
  checkcount: any;
  imagepath: any;
  UserType: any;
  UserSubjectlist: any;
  url
  daytimetable: any;
  UserDatahistory:any;
  dayArr;
  area:any;
  Days:any;
  checklist = [];
  allData=[];
  /* Time Table calander varaible ............................... */
  date: any;
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  monthNames: string[];
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  eventList: any;
  unfavicon:boolean;
  selectedEvent: any;
  isSelected: any;
  msg1: any;
  todayDate: any;
  ProfileData: any;
  level:any;
  type1:any;
  Subject:any;
  Price:any;
  Date:any;
  gender:any;
  /* ............................... jinny Varable  */
  UserDetails:any;
  BillDetails:any;
  User:any;
  city:any;
  constructor(private event: Events, public modalCtrl: ModalController, public datepipe: DatePipe, private calendar: Calendar, public alertCtrl: AlertController, public toastCtrl: ToastController, public LoadingCtrl: LoadingController, public navCtrl: NavController, public rest: RestProvider, public navParams: NavParams) {
    this.UserDetails=JSON.parse(localStorage.getItem('User')); 
    this.User = this.UserDetails.name;
    this.address = this.UserDetails.address;
    this.city = this.UserDetails.city;
    this.gender = this.UserDetails.gender;
    this.age = this.UserDetails.age
    this.getBill();

    this.pet = 'subject';
    
   // this.grttimetable()
  }

  /*  time table ...................... */

// jinny api
gotoedit() {
  this.navCtrl.push(EditProfilePage, { image: this.image, name: this.name, address: this.address, contactnumber: this.contactnumber, age: this.age, description: this.biography })
}
 getBill() {
  /// alert(this.UserDetails._id)
  // var User = JSON.parse(this.UserDetails)
  // console.log('User Details >>>>>>>>',User._id)
  if (this.rest.networkCheck == true) {
      var postData = {      
        UserID: this.UserDetails._id           
      };     
      this.rest.getBillDetails((postData)).then((result) => {    
        if(result['status']) {
          this.sujectavailable = true;
          this.BillDetails = result['data']
        } 
      
      
      }, (err) => {
      });
    }
    else {
      this.rest.checkNetwork();
    }
  }

 /////
}
