import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest";
/**
 * Generated class for the TermsConditionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-terms-condition',
  templateUrl: 'terms-condition.html',
})
export class TermsConditionPage {
message:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, public rest:RestProvider) {
  }

  ionViewDidLoad() {
   this.rest.terms().then((result)=>{
     this.message=result['message'];
   },err=>{

   })
  }

}
