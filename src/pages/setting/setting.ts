import { Component } from '@angular/core';
import {AlertController, App, IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';
import {LoginPage} from "../login/login";
import {EditProfilePage} from "../edit-profile/edit-profile";
import {ChangepassPage} from "../changepass/changepass";

/**
 * Generated class for the SettingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-setting',
  templateUrl: 'setting.html',
})
export class SettingPage {

  constructor(public navCtrl: NavController,public alertCtrl: AlertController,public app : App,public loadingCtrl: LoadingController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SettingPage');
  }
  logout(){
    let alert = this.alertCtrl.create({
      title: 'Confirm Message',
      enableBackdropDismiss: false,
      message: 'Do you wish to Logout ?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            let loader = this.loadingCtrl.create({
              content: "Please wait...",
              duration:1500
            });
            localStorage.clear();
            this.app.getRootNav().setRoot(LoginPage);
            loader.present();
          }
        },
        {
          text: 'No',
          handler: () => {
          }
        }
      ]
    });
    alert.present();
  }
  gotoedit() {
    this.navCtrl.push(EditProfilePage)
  }
  gotochangePass()
  {
    this.navCtrl.push(ChangepassPage);
  }
}
