import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController ,Platform} from 'ionic-angular';
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest";
import { StatusBar } from '@ionic-native/status-bar';
/**
 * Generated class for the ForgotPasswordPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class ForgotPasswordPage {
  authForget: FormGroup;
  credential: { 'user_email': any } = { user_email: '' };
  email: AbstractControl;
  public lang;
  constructor(private platform: Platform,private statusBar: StatusBar,public alertCtrl: AlertController, public rest: RestProvider, public loadingCtrl: LoadingController, public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams) {
    this.lang = this.rest.Lang;
    this.authForget = formBuilder.group({
      email: ['', Validators.compose([Validators.required])],
      // email: ['', Validators.compose([Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
    });
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#13364f');
      statusBar.styleDefault();
    
    });
  }

  SignIn() {
    this.navCtrl.pop();
  }
  forgotpassword() {
    if (this.rest.networkCheck == true) {
      let loader = this.loadingCtrl.create({
        content: "Loading..."
      });
      loader.present();
      let postData = {
        email: this.credential.user_email,

      };
      this.rest.passwordget((postData)).then((result) => {
        if (result['status'] == true ) {
          let alert = this.alertCtrl.create({
            title: 'Your Password',
            message: result['message'],
            cssClass: 'arrow-account',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {

                  console.log('Cancel clicked');
                }
              }

            ]
          });
          alert.present();
          loader.dismiss();
        }
        else if (result['status'] == false && result['response_code'] == 'NO_FOUND_USER') {
          let alert = this.alertCtrl.create({
            title: 'Password',
            message: result['message'],
            cssClass: 'arrow-account',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {

                  console.log('Cancel clicked');
                }
              }

            ]
          });
          alert.present();
          loader.dismiss();
        }
      }, (err) => {
        console.log(err.status);
        if (err.status == 405) {
          alert("Internal Serrver Error");
          loader.dismiss();
        }
        if (err.status == 0) {
          alert("Internal Serrver Error");
          loader.dismiss();
        }
      });
    }
    else {
      this.rest.checkNetwork();
    }

  }
  close() {
    this.navCtrl.pop();
  }
}
