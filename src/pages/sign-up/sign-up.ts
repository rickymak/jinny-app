import { Component } from '@angular/core';
import {
    AlertController, App, normalizeURL, IonicPage, NavController, NavParams,
    ActionSheetController, LoadingController, Platform, ToastController
} from 'ionic-angular';
import { AngularFireAuth } from 'angularfire2/auth';
import { IonicSelectableComponent } from 'ionic-selectable';
import { DomSanitizer, EVENT_MANAGER_PLUGINS } from '@angular/platform-browser';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators, } from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest";
import { WelcomePage } from "../welcome/welcome";
import { LoginPage } from "../login/login";
import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook';
import { HomePage } from '../home/home';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Crop } from '@ionic-native/crop';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer';
import { ChangeDetectorRef } from '@angular/core';
import { FilePath } from '@ionic-native/file-path';
import { SelectAccountPage } from "../select-account/select-account";
import { GooglePlus } from '@ionic-native/google-plus';
import { TermsConditionPage } from "../terms-condition/terms-condition";
import { PrivacyPage } from "../privacy/privacy";
import { StatusBar } from '@ionic-native/status-bar';
import { OneSignal } from '@ionic-native/onesignal';

@IonicPage()

@Component({
    selector: 'page-sign-up',
    templateUrl: 'sign-up.html',
})
export class SignUpPage {
    requestData:any=[];
    CityData:any=[];
    State:any;
    SelectState:any=[];
    public lang;
    addArr: any;
    name: AbstractControl;
    email: AbstractControl;
    mob: AbstractControl;
    password: AbstractControl;
    address: AbstractControl;
    city: AbstractControl;
    location:AbstractControl;
    Confirmpassword: string = '';
    authForm: FormGroup;
   // city: any;
    UserType: any;
    UserData: any;
    userData: any;
    loginID: any;
    imageURI: string = 'assets';
    filename: string = '';
    imageData: any;
    fbemail: any;
    url: any;
    freeTution: boolean;
    checked: boolean;
   // location:any;
    signup: { fullname: String, email: string, contactnumber: string, password: string, gender: string, age: string, address: string ,city:string,location:string} = { fullname: '', email: '', contactnumber: '', password: '', gender: '', age: '', address: '',city:'',location:'' };
    constructor(public oneSignal:OneSignal,private statusBar: StatusBar,public googlePlus: GooglePlus, public loadingCtrl: LoadingController, public toastCtrl: ToastController, private filePath: FilePath, private platform: Platform, private transfer: FileTransfer, private crop: Crop, private camera: Camera, public actionSheetCtrl: ActionSheetController, private facebook: Facebook, public afireauth: AngularFireAuth, public app: App, public alertCtrl: AlertController, public rest: RestProvider, public formBuilder: FormBuilder, public navCtrl: NavController, public navParams: NavParams) {
        this.lang = this.rest.Lang;
        this.authForm = formBuilder.group({
            name: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
            email: ['', Validators.compose([Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
            mob: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
            password: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(16), Validators.pattern("^[a-zA-Z0-9_!@#$&()-`.+,/\"]*$")]),
            Confirmpassword: ['', [Validators.required]],
            address: ['', Validators.compose([Validators.required])],
            city: ['', Validators.compose([Validators.required])],
            location: ['', Validators.compose([Validators.required])],
            age: new FormControl('', [Validators.required, Validators.minLength(1), Validators.maxLength(3)]),
            gender: ['', Validators.compose([Validators.maxLength(30), Validators.pattern('[a-zA-Z]*'), Validators.required])],
        }, { validator: this.matchingPasswords('password', 'Confirmpassword') });

        this.UserType = localStorage.getItem("UserType");
        this.url = this.rest.imageUrl;
        this.navParams.get('from')

        platform.ready().then(() => {
            statusBar.backgroundColorByHexString('#13364f');
            statusBar.styleDefault();        
          });
          this.CityList();
    }
    CityList() {
         this.rest.City().then(
           (result: any) => {
            this.State = result['City']['states']
            console.log('state>>>>>',this.State) 
           }, (err) => {       
             if (err.status == 405 || err.status == 500 || err.status == 0) {    
             }
       
           }
         );
       }
   
    portChange(event: {
        component: IonicSelectableComponent,
        value: any
    }) {
      
        this.SelectState = event.value['districts']       
        for (let i = 0; i < this.SelectState.length; i++) {
            this.requestData = {             
              "Name": this.SelectState[i]
            } 
            this.CityData.push(this.requestData);
          //  console.log('Get this.CityData',this.CityData)           
          }

    }

 
    login() {
        this.navCtrl.pop();
    }
    matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let password = group.controls[passwordKey];
            let confirmPassword = group.controls[confirmPasswordKey];

            if (password.value !== confirmPassword.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }
    presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Choose Image',
            buttons: [
                {
                    text: 'Camera',
                    // role: 'destructive',
                    handler: () => {
                        this.openCamera();
                    }
                }, {
                    text: 'Gallery',
                    handler: () => {
                        this.openGallery();
                    }
                }, {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }
    openCamera() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 1000,
            targetHeight: 1000
        }

        this.camera.getPicture(options).then((imageData) => {
            console.log(imageData);
            this.crop.crop(imageData, { quality: 100, targetWidth: 600, targetHeight: 600 })
                .then(
                    newImage => {
                        console.log("newImage" + newImage);
                        if (this.platform.is('android')) {
                            let str1 = newImage.substring(0, newImage.indexOf('?'));
                            this.imageURI = str1;
                        } else {
                            this.imageURI = normalizeURL(newImage);
                        }

                    },
                    error => console.error('Error cropping image', error)
                );
        }, (err) => {
            console.log("error" + err);
        });
    }
    getImage() {
        const options: CameraOptions = {
            quality: 100,
            sourceType: 2,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        }

        this.camera.getPicture(options).then((imageData) => {
            this.imageURI = imageData;

        }, (err) => {
            console.log(err);
        });
    }
    openGallery() {
        const options: CameraOptions = {
            quality: 100,
            sourceType: 2,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true
        }

        this.camera.getPicture(options).then((imageData) => {
            var str;
            if (this.platform.is('android')) {
                str = imageData.substring(0, imageData.indexOf('?'));
            } else {
                str = imageData;
            }
            this.crop.crop(str, { quality: 100, targetWidth: 600, targetHeight: 600 })
                .then(
                    newImage => {
                        console.log("newImage" + newImage);
                        if (this.platform.is('android')) {
                            let str1 = newImage.substring(0, newImage.indexOf('?'));
                            this.imageURI = str1;
                        } else {
                            this.imageURI = normalizeURL(newImage);
                        }

                    },
                    error => console.error('Error cropping image', error)
                );
        }, (err) => {
        });
    }
    uploadFile() {
        console.log('this.imageURI>>',this.imageURI)
        const fileTransfer: FileTransferObject = this.transfer.create();
        this.filename = "Profile" + Math.round((Math.random() * 100) * 100) + ".jpg";
        let options: FileUploadOptions = {
            fileKey: 'profileImage',
           fileName: this.filename,   
            chunkedMode: false,
           // mimeType: "image/jpeg",  
            mimeType: 'multipart/form-data', 
        }

        console.log("ajhdgajh", this.rest.imageUrl)

        fileTransfer.upload(this.imageURI, this.rest.imageUrl, options)
            .then((data) => {
                alert(JSON.stringify("data saved" + data));
            }, (err) => {
console.log(err)
alert(JSON.stringify("error>>"+ err));
            });
    }
    Signup(myForm) {
        if (this.checked == false || this.checked == undefined) {
            let alert = this.alertCtrl.create({
                title: 'Message',
                message: 'Please check the Terms and Conditions checkbox',
                cssClass: 'arrow-account',
                buttons: [
                    {
                        text: 'Ok',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    }

                ]
            });
            alert.present();
        }
        else {
            if (this.rest.networkCheck == true) {
                let loader = this.loadingCtrl.create({
                    content: "Loading..."
                });
                loader.present();

                this.afireauth.auth.createUserWithEmailAndPassword(this.signup.email, '1234567').then((data) => {

                    this.UserData = data.user.uid
                    this.saveUserData(loader)

                }).catch((data) => {

                    this.afireauth.auth.signInWithEmailAndPassword(this.signup.email, '1234567').then((methods) => {

                        console.log(methods.user.uid)
                        this.UserData = methods.user.uid
                        this.saveUserData(loader)
                    });
                })
            }
            else {
                this.rest.checkNetwork();
            }
        }
    }
    upload() {
        this.uploadFile();
    }
    loginWithFB() {
        let loader = this.loadingCtrl.create({
            content: "Loading..."
        });
        loader.present();
        this.facebook.login(['email', 'public_profile']).then((response: FacebookLoginResponse) => {
            this.facebook.api('me?fields=id,name,email,first_name,picture.width(720).height(720).as(picture_large)', [])
                .then(profile => {
                    this.userData = { userid: profile['id'], email: profile['email'], first_name: profile['first_name'], picture: profile['picture_large']['data']['url'], username: profile['name'], EmailVarifies: true }
                    if (profile.email == undefined) {
                        this.GetEmail(profile, loader, 0);
                        return;
                    }
                    this.loginWithFirebase(profile, loader, this.UserType, profile.id, 0, this.userData.EmailVarifies);
                })
                .catch((err) => {
                    console.log("this is error from facebook", err);
                });
        }).catch((err) => {
            console.log("this is login error", err);
        });

    }

    doGoogleLogin() {
        if (this.rest.networkCheck == true) {
            let nav = this.navCtrl;
            let loading = this.loadingCtrl.create({
                content: 'Please wait...'
            });
            loading.present();
            // this.googlePlus.login({
            //     'webClientId': '205339668335-l52buv9j3fs7t2bh4i7cp8nqe0vf0jq4.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
            //     'offline': true
            // })
            this.googlePlus.login({               
                //  'webClientId': '871193662003-iu5hqoarsj788m156jkojlu3s8e4p6ic.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                  'webClientId': '871193662003-ejt6f3nilgvem4k0abusu6bn9ig13rj7.apps.googleusercontent.com', // optional clientId of your Web application from Credentials settings of your project - On Android, this MUST be included to get an idToken. On iOS, it is not required.
                  'offline': true
              })
                .then((user) => {
                    //  debugger
                    console.log(JSON.stringify(user) + "google api data coming")
                    var password = '12345678'
                    var status = true
                    this.loginWithFirebase(user, loading, this.UserType, password, 1, status);

                }, (error) => {
                    console.log(error);
                    loading.dismiss();
                });
        }
        else {
            this.rest.checkNetwork();
        }
    }

    close() {
        this.navCtrl.pop();
    }
    GetEmail(profile, loader, authType) {
        let forgot = this.alertCtrl.create({
            title: 'Email address?',
            message: "Please Enter your email address to sign up.",
            inputs: [
                {
                    name: 'email',
                    placeholder: 'Email',
                    type: 'email'
                },
            ],
            buttons: [
                {
                    text: 'Cancel',
                    handler: data => {
                        console.log('Cancel clicked');
                        loader.dismiss();
                    }
                },
                {
                    text: 'Send',
                    handler: data => {

                        let validateObj = this.validateEmail(data);
                        if (!validateObj.isValid) {
                            this.showErrorToast('Email is Required');
                            loader.dismiss();
                            return false;
                        } else {
                            profile['email'] = data.email;
                            var email = false;
                            this.loginWithFirebase(profile, loader, this.UserType, profile.id, authType, email);
                        }
                    }
                }
            ]
        });
        forgot.present();
    }
    validateEmail(data) {
        if (/(.+)@(.+){2,}\.(.+){2,}/.test(data.email)) {
            return {
                isValid: true,
                message: ''
            };
        } else {
            return {
                isValid: false,
                message: 'Proper Email address is required'
            }
        }
    }
    showErrorToast(data: any) {
        let toast = this.toastCtrl.create({
            message: data,
            duration: 3000,
            position: 'bottom'
        });

        toast.onDidDismiss(() => {
            console.log('Dismissed toast');
        });

        toast.present();
    }

    loginWithFirebase(profile, loader, type, password, authType, email) {
        this.afireauth.auth.createUserWithEmailAndPassword(profile.email, '1234567').then((data) => {
            this.UserData = data.user.uid;
            console.log(this.UserData + "User ID");
            let postData = {
                sid: authType == 0 ? profile.id : profile.userId,
                u_id: this.UserData,
                fullname: authType == 0 ? profile.name : profile.displayName,
                email: profile.email,
                image: authType == 0 ? this.userData.picture : profile.imageUrl,
                emailverifier: email,
                contactnumber: '',
                password: '',
                type: authType == 0 ? 'facebook' : 'google',
                gender: '',
                age: '',
                address: '',
                isFree: this.freeTution == true ? this.freeTution = true : this.freeTution = false
            };
            this.sendDataToServer(postData, type, loader);
  
    }).catch((data) => {

        this.afireauth.auth.signInWithEmailAndPassword(profile.email, '1234567').then((methods) => {
            console.log(methods.user.uid)
            this.UserData = methods.user.uid
         //   this.saveUserData(loader)
         let postData = {
                        sid: authType == 0 ? profile.id : profile.userId,
                        u_id: this.UserData,
                        fullname: authType == 0 ? profile.name : profile.displayName,
                        email: profile.email,
                        image: authType == 0 ? this.userData.picture : profile.imageUrl,
                        emailverifier: email,
                        contactnumber: '',
                        password: '',
                        type: authType == 0 ? 'facebook' : 'google',
                        gender: '',
                        age: '',
                        address: '',
                        isFree: this.freeTution == true ? this.freeTution = true : this.freeTution = false
                    };

                    this.sendDataToServer(postData, type, loader);
        });
    })
    }

    sendDataToServer(postData, type, loader) {
         this.oneSignal.getIds().then(pushids => {
            postData['oneSingal'] =  pushids.userId;
        let userType = type == 'Trainer' ? "1" : "2";
        this.rest.Registration(JSON.stringify(postData), userType).then((result) => {
            if (result['status'] == true && (result['response_code'] == 'USER_INSERT' || result['response_code'] == 'USER_EXISTS')) {
                var data = result['data'];               
                var setting_data = result['setting_data'];
                localStorage.setItem('ammount', setting_data['upgrade_plan']);
                type = localStorage.getItem('UserType') == "Trainer" ? "1" : "2";
                if (type == data['user_type']) {
                    loader.dismiss();
                    let id = result['response_code'] == 'USER_INSERT' ? data["id"] : data["user_id"];
                    localStorage.setItem('UserEmail', data['email']);
                    localStorage.setItem('id', result['id']);                  
                    localStorage.setItem('UserName', data['fullname']);
                    localStorage.setItem('UserType', data['user_type']);
                    localStorage.setItem('image', data['image']);
                    localStorage.setItem('status', "0");
                    this.app.getRootNav().setRoot(HomePage);
                }
                else {
                    const toast = this.toastCtrl.create({
                        message: 'Email Id Used In Other Account ',
                        duration: 3000
                    });
                    toast.present();
                    loader.dismiss();
                    return;
                }
            }
            if (result['status'] == false && result['response_code'] == 'EMAIL_EXIT') {
                var setting_data = result['setting_data'];
                localStorage.setItem('ammount', setting_data['upgrade_plan']);
                var data = result['data'];             
                localStorage.setItem('UserEmail', data['email']);
                localStorage.setItem('id',  data['user_id']);
                localStorage.setItem('UserName', data['fullname']);
                localStorage.setItem('UserType', data['user_type']);
                localStorage.setItem('image', data['image']);
                localStorage.setItem('status', "0");
                localStorage.setItem('UserFireBaseID', data['u_id']);
                loader.dismiss(); 
                this.app.getRootNav().setRoot(HomePage);
            }
            if (result['status'] == true && result['response_code'] == 'USER_INSERT_REFUTED') {
                const toast = this.toastCtrl.create({
                    message: result['message'],
                    duration: 3000
                });
                toast.present();
                loader.dismiss();
                this.app.getRootNav().setRoot(LoginPage);
            }
            if (result['status'] == false && result['response_code'] == 'EMAIL_NOT_VERIFIED') {
                const toast = this.toastCtrl.create({
                    message: result['message'],
                    duration: 3000
                });
                toast.present();
                loader.dismiss();
                this.app.getRootNav().setRoot(LoginPage);
            }
            if (result['status'] == false && result['response_code'] == 'BLOCKED_USER') {
                const toast = this.toastCtrl.create({
                    message: result['message'],
                    duration: 3000
                });
                toast.present();
                loader.dismiss();
                this.app.getRootNav().setRoot(LoginPage);
            }
            console.log("Facebook login api success");
        }, (err) => {
            if (err.status == 405 || err.status == 500 || err.status == 0) {
                const toast = this.toastCtrl.create({
                    message: 'Internal Server Error' + '-' + err['status'],
                    duration: 3000
                });
                toast.present();
                loader.dismiss();
            }
            console.log(err);
        });
    }).catch((err)=>{
        console.log(err)
    })
    }
    openTerms() {
        this.navCtrl.push(TermsConditionPage);
    }
    policy() {
        this.navCtrl.push(PrivacyPage);
    }

    saveUserData(loader) {
        // if (this.imageURI != 'assets') {
        //     this.uploadFile();
        // }
        console.log('this is add' + this.signup.address['id']);
        let postData = {
            name: this.signup.fullname,
            email: this.signup.email,
            phone: this.signup.contactnumber,
            password: this.signup.password,
            type: 'Email',
            UserType: localStorage.getItem('UserType'),
            gender: this.signup.gender,
          //  image: this.filename,
            age: this.signup.age,
            state: this.signup.address['state'],
            city: this.signup.city['Name'],
            address: this.signup.location,
           // isFree: this.freeTution == true ? this.freeTution = true : this.freeTution = false
        };
        console.log(postData);
       
        this.rest.Registration((postData), '').then((result) => {
            console.log(result);
            if (result['status']) {
                let alert = this.alertCtrl.create({
                    title: 'Message',
                    message: result['message'],
                    cssClass: 'arrow-account',
                    buttons: [
                        {
                            text: 'Ok',
                            role: 'cancel',
                            handler: () => {
                                this.app.getRootNav().setRoot(LoginPage);
                                console.log('Cancel clicked');
                            }
                        }
                    ]
                });
                alert.present();
                loader.dismiss();
            }
            if (result["status"] == false) {
                let alert = this.alertCtrl.create({
                    title: 'Message',
                    message: this.signup.email + ' already exists',
                    cssClass: 'arrow-account',
                    buttons: [
                        {
                            text: 'Ok',
                            role: 'cancel',
                            handler: () => {
                            }
                        }
                    ]
                });
                alert.present();
                loader.dismiss();
            }
        }, (err) => {

            if (err.status == 405 || err.status == 500 || err.status == 0) {
                const toast = this.toastCtrl.create({
                    message: 'Internal Server Error' + '-' + err['status'],
                    duration: 3000
                });
                toast.present();
                loader.dismiss();
            }
        });
    }

}

