import { Component } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import {
  ActionSheetController,
  AlertController,
  App,
  IonicPage,
  LoadingController,
  NavController,
  NavParams, normalizeURL, Platform,
  ToastController,
  Events
} from 'ionic-angular';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { FileTransfer, FileTransferObject, FileUploadOptions } from "@ionic-native/file-transfer";
import { Crop } from "@ionic-native/crop";

/**
* Generated class for the EditProfilePage page.
*
* See https://ionicframework.com/docs/components/#navigation for more info on
* Ionic pages and navigation.
*/

@IonicPage()
@Component({
  selector: 'page-edit-profile',
  templateUrl: 'edit-profile.html',
})
export class EditProfilePage {
  CityData:any=[
    {Name: "Araria"},
    {Name: "Arwal"},
    {Name: "Aurangabad"},
    {Name: "Banka"},
    {Name: "Begusarai"},
    {Name: "Bhagalpur"},
    {Name: "Bhojpur"},
    {Name: "Buxar"},
  ];
  
  name: AbstractControl;
  SelectState:any=[];
  mob: AbstractControl;
  password: AbstractControl;
  address1: AbstractControl;
  address2:AbstractControl;
  description: AbstractControl;
  Confirmpassword: string = '';
  authForm: FormGroup;
  signup: { fullname: string, contactnumber: string, gender: string, age: string, address: string, desc: string ,city1: string ,address2:string} = { fullname: '', contactnumber: '', gender: localStorage.getItem('Gender'), age: '', address: '', desc: '' ,city1:'',address2:''};
  UserType: any;
  UserData: any;
  email = localStorage.getItem('UserEmail');
  userData: any;
  loginID: any;
  imageURI: string = 'assets';
  checkImage = 'assets';
  filename: string = '';
  imageData: any;
  city: any = [];
  img: any;
  type = localStorage.getItem('UserType');
  public lang;

  requestData:any=[];
  constructor(private event: Events, public navCtrl: NavController, public actionSheetCtrl: ActionSheetController, private platform: Platform, private transfer: FileTransfer, private crop: Crop, private camera: Camera, public app: App, public loadingCtrl: LoadingController, public alertCtrl: AlertController, public rest: RestProvider, public toastCtrl: ToastController, public formBuilder: FormBuilder, public navParams: NavParams) {
  // this.CityList();
    this.lang = this.rest.Lang;
    var that = this;
    this.getAdd(function (isSucess) {
      if (isSucess) {
       that.fetchData();
      }
    });
    this.authForm = formBuilder.group({
      name: ['', Validators.compose([Validators.maxLength(30), Validators.required])],
      mob: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(12)]),
      address1: new FormControl('', [Validators.required]),
      address2: new FormControl('', [Validators.required]),
      age: new FormControl('', [Validators.required]),
     // description: new FormControl('', [Validators.required]),
      gender: new FormControl('', [Validators.required]),
      city: new FormControl('', [Validators.required]),
    });
  
  }
  getAdd(next) {   
    // this.spinnerService.show()
     this.rest.City().then(
       (result: any) => {
        this.city = result['City']['states']  
       // console.log('show state data >>>>>>>',this.city)
        // this.SelectState = this.city['districts']       
        // for (let i = 0; i < this.SelectState.length; i++) {
        //     this.requestData = {             
        //       "Name": this.SelectState[i]
        //     } 
        //     this.CityData.push(this.requestData);
        //   //  console.log('Get this.CityData',this.CityData)           
        //   }
        next(true)
       
       }, (err) => {
   
         if (err.status == 405 || err.status == 500 || err.status == 0) {    
         }
   
       }
     );
   }
  Update(tt) {
  //  console.log(JSON.parse(localStorage.getItem('User')))
    var getUser = JSON.parse(localStorage.getItem('User'))
    console.log(getUser._id)
    if (this.rest.networkCheck == true) {
      let loader = this.loadingCtrl.create({
        content: "Loading..."
      });
     // loader.present();
      let postData = {
        postId: getUser._id,
        fullname: this.signup.fullname,
        contactnumber: this.signup.contactnumber,      
        gender: this.signup.gender,
        image: this.filename,
        age: this.signup.age,
        state: this.signup.address['state'],
        address: this.signup.address2,
        city:this.signup.city1
      
      };
console.log('postdata>>>>>>>>',postData)

      this.rest.profileUpdate(postData).then((result) => {
        if (result['status'] == true) {

          let alert = this.alertCtrl.create({
            title: 'Message',
            message: result['message'],
            cssClass: 'arrow-account',
            buttons: [
              {
                text: 'Ok',
                role: 'cancel',
                handler: () => {               
                  localStorage.setItem('UserName', result['data']['fullname']);
                  this.event.publish("reloadProfile");
                  this.navCtrl.pop();
                  console.log('Cancel clicked');
                }
              }

            ]
          });
          alert.present();
          loader.dismiss();
        }
        else if (result['status'] == false && result['response_code'] == 'UPDATE_FAIL') {
          loader.dismiss();
        }
        else if (result['status'] == false && result['response_code'] == 'NO_FOUND_USER') {
          loader.dismiss();
        }
      }, (err) => {
        console.log(err);
        if (err.status == 405 || err.status == 500 || err.status == 0) {
          const toast = this.toastCtrl.create({        
            message: 'Internal Server Error' + '-' + err['status'],
            duration: 3000
          });
          toast.present();
          loader.dismiss();
        }
      });
    }
    else {
      this.rest.checkNetwork();
   }
  }

 


  fetchData() {   
    if (this.rest.networkCheck == true) {
      let loader = this.loadingCtrl.create({
        content: "Loading..."
      });
      loader.present();
      var postData = {
        postId:"5e0a2887d9c6754e50495324",
      };
      this.rest.fetchUserData((postData)).then((result) => {
        if (result['status']) {
          this.signup.fullname = result['data'].name;       
          for (var j = 0; j < this.city.length; j++) {
            if (this.city[j].state == result['data'].state) {
              // this.signup.address = this.city[j]['state'];
              this.signup.address = this.city[j];
             // console.log( this.signup.address)
             var CityData1 = this.city[j]
              //console.log('get City>>>>>>',CityData1['districts'])
             // console.log('get City>>>>>>',CityData1['districts'].length)

                this.SelectState = CityData1['districts']       
        for (let i = 0; i < this.SelectState.length; i++) {
            this.requestData = {             
              "Name": this.SelectState[i]
            } 
            this.CityData.push(this.requestData);
          // console.log('Get this.CityData',this.CityData)           
          }
              
            }
          }
          this.signup.contactnumber = result['data'].phone;
          this.signup.age = result['data'].age;
         this.email = result['data'].email;
         this.signup.gender = result['data'].gender;
         this.signup.city1 = result['data'].city;
         this.signup.address2= result['data'].address,
      
          loader.dismiss();
        }
        else {
          loader.dismiss();
        }

      }, (err) => {
        loader.dismiss();
      });
    }
    else {
      this.rest.checkNetwork();
    }
  }
  portChange(event: {
    component: IonicSelectableComponent,
    value: any
}) {
  this.CityData=[];
    this.SelectState = event.value['districts']       
    for (let i = 0; i < this.SelectState.length; i++) {
        this.requestData = {             
          "Name": this.SelectState[i]
        } 
        this.CityData.push(this.requestData);
      // console.log('Get this.CityData',this.CityData)           
      }

}
}
