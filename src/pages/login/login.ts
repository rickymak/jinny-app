import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, Events, App, LoadingController, Platform, AlertController } from 'ionic-angular';
import { SignUpPage } from "../sign-up/sign-up";
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";
import { RestProvider } from "../../providers/rest/rest"
import { ForgotPasswordPage } from "../forgot-password/forgot-password";
import { HomePage } from "../home/home";
import { SelectAccountPage } from "../select-account/select-account";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

declare var window;
@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  authLogin: FormGroup;
  time1: any;
  credential: {  'password': any, 'pushId': any } = {  password: '', pushId: '' };
  password: AbstractControl;
  email: AbstractControl;
  public lang;
  msg: any;
  fireBaseId: any;
  userData: any;
  UserData: any;
  FBUI: any
  constructor(public alertCtrl: AlertController, private statusBar: StatusBar, private splashScreen: SplashScreen, private platform: Platform, public app: App, public loadingCtrl: LoadingController, public events: Events, public navCtrl: NavController, public toastCtrl: ToastController, public navParams: NavParams, public formBuilder: FormBuilder, public rest: RestProvider) {
    platform.ready().then(() => {
      statusBar.backgroundColorByHexString('#13364f');
      statusBar.styleDefault();
      splashScreen.hide();
    });
   
    this.lang = this.rest.Lang;
    this.authLogin = formBuilder.group({
       email: ['', Validators.compose([Validators.required, Validators.minLength(10)])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  onLogin() {
    // this.navCtrl.setRoot(HomePage);
    // return
    let that = this;
    if (that.rest.networkCheck == true) {
     // this.oneSignal.getIds().then(pushids => {
      let loading = that.loadingCtrl.create({
        content: 'Please wait...'
      });

      loading.present();
      let postParams = {
        phone: that.email,
        password: that.password,      
      }
console.log('postParams',postParams)
      that.rest.login(postParams).then((result) => {
        if (result['ResponeCode'] == 'Successful') {
         
          var data = (result['data']);    
          if(data['UserType'] == 'User') {
            localStorage.setItem('User', JSON.stringify(result['data']));
            that.navCtrl.setRoot(HomePage);
            loading.dismiss();
            const toast = that.toastCtrl.create({
              message: result['message'],
              duration: 3000
            });
            toast.present();
          }
          loading.dismiss();
        }
        else if (!result['status'] ) {
          const toast = that.toastCtrl.create({
            message: result['message'],
            duration: 3000
          });
          toast.present();
          loading.dismiss();
        }
    
      }, (err) => {
        loading.dismiss();

        console.log(JSON.stringify(err) + "check error");
        if (err.status == 500) {
          const toast = that.toastCtrl.create({
            message: err.status,
            duration: 3000
          });
          toast.present();
        }
  });
  //  });
    }
    else {
      that.rest.checkNetwork();
    }

  }
  forgotPasswordPage() {
    this.navCtrl.push(ForgotPasswordPage);
  }
  selectaccount() {
    this.navCtrl.push(SelectAccountPage);
  }


 


  showErrorToast(data: any) {
    let toast = this.toastCtrl.create({
      message: data,
      duration: 3000,
      position: 'bottom'
    });

    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });

    toast.present();
  }
  validateEmail(data) {
    if (/(.+)@(.+){2,}\.(.+){2,}/.test(data.email)) {
      return {
        isValid: true,
        message: ''
      };
    } else {
      return {
        isValid: false,
        message: 'Proper Email address is required'
      }
    }
  }
  EnterApp(){
    this.navCtrl.setRoot(HomePage);
  }
}
