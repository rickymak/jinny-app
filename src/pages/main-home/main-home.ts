import {Component, ViewChild} from '@angular/core';
import {
  IonicPage,
  NavController,
  NavParams,
  ModalController,
  ToastController,
  ModalOptions,
  LoadingController,
  Modal,
  Platform, AlertController, Tabs,Slides 
} from 'ionic-angular';
import {RestProvider} from "../../providers/rest/rest"
import {GroupSubjectPage} from "../group-subject/group-subject";

import { Content } from 'ionic-angular';

import {ProfilePage} from "../profile/profile";

import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import {LoginPage} from "../login/login";
/**
 * Generated class for the MainHomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-main-home',
  templateUrl: 'main-home.html',
})
export class MainHomePage {
  @ViewChild('myTabs') tabRef: Tabs;
  @ViewChild(Content) content: Content;
  @ViewChild('Slides') slides: Slides;
    choice:any;
    data1:any;
    id:any;
    level:any;
  opend:boolean;
    GetLevel:any;
   view:boolean=true;
    myModelOption:any;
    url:any;
    searchQuery:any;
  notFound:boolean=false;
    GetAdvertisement:any;
    UserData:any;
    checkcount:any;
    addUrl:any;
    user:any;
  datacheck:any[]=[];
  page_count:number=0;
  url1:any;
  url2:any;

  //////   jinny varaible declare
  MainCategory:any;
  unregisterBackButtonAction:any;
  constructor(public iab: InAppBrowser,private toastCtrl: ToastController,public platform:Platform,public loadingCtrl: LoadingController ,public rest:RestProvider ,public navCtrl: NavController, public navParams: NavParams,public modalCtrl: ModalController,public alert:AlertController) {
      this.choice="sale";
      this.MainCategoryList()
  } 
    gotoviewcar(title,id){            
        localStorage.setItem('gruoptitle',title);
        localStorage.setItem('MainCategoryId',id);
        this.navCtrl.push(GroupSubjectPage);
    }
  


MainCategoryList() {   
  let loader = this.loadingCtrl.create({
    content: "Loading..."
  });
  loader.present(); 
  this.rest.getMainCat().then(
    (result: any) => {
      if (result.status &&  result.ResponeCode == 'Successful') {
        loader.dismiss();
        this.MainCategory = result.data ;         
      }
      else {
        loader.dismiss();
        alert("data not found")
      }
    
    }, (err) => {

      if (err.status == 405 || err.status == 500 || err.status == 0) {    
      }

    }
  );
}

}











