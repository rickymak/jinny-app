import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController, App, ToastController } from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest"
import { HomePage } from '../home/home';
import {SignUpPage} from '../sign-up/sign-up'
import { AbstractControl, FormBuilder, FormGroup, Validators } from "@angular/forms";




@Component({
  selector: 'page-addsubject',
  templateUrl: 'addsubject.html',
})
export class AddsubjectPage {
  User: any;
  GetLevel: any;
  Alldata_Available: boolean = false;
  data_Available: boolean = false;
  AgeGroup: any = '';
  GetLevelSubCategory: any;
  validation: any;
  addSubjectForm: FormGroup;
  GroupSubject: any = '';
  SubCategory: any;
  Subject: any = '';
  Countrylist: any;
  description: any;
  Country: any;
  other: any = '';
  Session: any = '';
  day: any = [];
  ageGroup1: AbstractControl;
  groupSubject: AbstractControl;
  Subject1: AbstractControl;
  Country1: AbstractControl;
  pricePerSession: AbstractControl;
  description1: AbstractControl;
  pricePerMonth: AbstractControl;
  time1: AbstractControl;
  month: any = '';
  time: any = '';
  toTime: any = '';
  fromTime: any = '';
  toMin = "01:01";
  public lang;
  constructor(public loadingCtrl: LoadingController, private toastCtrl: ToastController, public app: App, public alertCtrl: AlertController, public formBuilder: FormBuilder, public LoadingCtrl: LoadingController, public rest: RestProvider, public navCtrl: NavController, public navParams: NavParams) {
    this.User = JSON.parse(localStorage.getItem("User"));
    console.log('USer>>>>>',this.User)
    this.lang = this.rest.Lang;
    this.addSubjectForm = formBuilder.group({
      ageGroup1: ['', Validators.compose([Validators.required])],
      groupSubject: ['', Validators.compose([Validators.required])],
      //   Subject1: ['', Validators.compose([Validators.required])],
      //  pricePerSession: ['', Validators.compose([Validators.required])],
      //  pricePerMonth: ['', Validators.compose([Validators.required])],
      description1: ['', Validators.compose([Validators.required])],
    });

  }

  ionViewWillEnter() {
    //  this.addSubjectForm.reset();
    this.getlevel();
    //  this.getCountrylist();
    this.AgeGroup = '';
    this.GroupSubject = '';
    this.Subject = '';
    this.Country = '';
    this.Session = '';
    this.month = '';
    this.other = '';
    this.day = [];
    this.fromTime = '';
    this.toTime = '';
    this.description = '';
  }
  getlevel() {
    if (this.rest.networkCheck == true) {
      let loader = this.loadingCtrl.create({
        content: "Loading ..."
      });
      loader.present();
      this.rest.getMainCat().then(
        (result) => {

          if (result['status'] == true) {
            loader.dismiss();
            this.GetLevel = (result["data"]);
          }
        }, (err) => {
          loader.dismiss()
          console.log(err.status);
          if (err.status == 405 || err.status == 500 || err.status == 0) {
            loader.dismiss()
            const toast = this.toastCtrl.create({
              message: 'Internal Server Error' + '-' + err['status'],
              duration: 3000
            });

          }

        }
      );
    }
    else {
      this.rest.checkNetwork();
    }
  }
  onContextChange(value): void {
    this.data_Available = true;
    if (this.rest.networkCheck == true) {
      let loader = this.LoadingCtrl.create({
        content: "Loading..."
      });
      loader.present();
      let postData = {
        postId: this.AgeGroup,

      };
      this.rest.subCatById((postData)).then((result) => {
        if (result['status'] == true) {
          this.GetLevelSubCategory = (result["data"]);
          this.Alldata_Available = false;
          loader.dismiss();
        }
        else if (result['status'] == false && result['response_code'] == 'NOT_AVAILABLE') {
          this.validation = 'NotFound';
          loader.dismiss();
        }
      }, (err) => {
        console.log(err.status);
        if (err.status == 405 || err.status == 500 || err.status == 0) {
          const toast = this.toastCtrl.create({
            message: 'Internal Server Error' + '-' + err['status'],
            duration: 3000
          });
          toast.present();
          loader.dismiss();
        }
      });
    }
    else {
      this.rest.checkNetwork();
    }
  }

  onContextChangeGroup(value): void {
    if (this.GroupSubject == '') {
    }

    if (this.rest.networkCheck == true) {
      let loader = this.LoadingCtrl.create({
        content: "Loading..."
      });
      loader.present();
      let postData = {
        subjectCategory_id: this.GroupSubject,
        title: '',
        level_id: this.AgeGroup,
        type: localStorage.getItem("UserType"),
        subjectid: '',
        username: ''
      };
      this.rest.GetSubject1(JSON.stringify(postData)).then((result) => {
        if (result['status'] == true && result['response_code'] == 'SUCCESS') {
          this.SubCategory = (result["data"]);
          loader.dismiss();
          this.Alldata_Available = true;
        }
        else if (result['status'] == false && result['response_code'] == 'NOT_AVAILABLE') {
          loader.dismiss();
        }
      }, (err) => {
        console.log(err.status);
        if (err.status == 405 || err.status == 500 || err.status == 0) {
          const toast = this.toastCtrl.create({
            message: 'Internal Server Error' + '-' + err['status'],
            duration: 3000
          });
          toast.present();
          loader.dismiss();
        }
      });
    }
    else {
      this.rest.checkNetwork();
    }
  }
  getCountrylist() {
    if (this.rest.networkCheck == true) {
      this.rest.getCountry().then(
        (result) => {
          if (result['status'] == true && result['response_code'] == 'SUCCESS') {
            this.Countrylist = (result["data"]);
          }
        }, (err) => {
          console.log(err.status);
          if (err.status == 405 || err.status == 500 || err.status == 0) {
            const toast = this.toastCtrl.create({
              message: 'Internal Server Error' + '-' + err['status'],
              duration: 3000
            });
            toast.present();
          }
        });
    }
    else {
      this.rest.checkNetwork();
    }
  }



  updateMin() {
    let min: any = parseInt(this.fromTime.split(":")[0]);
    let sec: any = parseInt(this.fromTime.split(":")[1]);
    min = sec == 59 ? min + 1 : min;
    sec = sec == 59 ? "00" : sec + 1;
    min = min < 10 ? "0" + min : min;
    this.toMin = min + ":" + ((sec < 10) ? ("0" + sec) : sec);
    this.toTime = this.toMin;
  }

  palceholder() {
    return 'Select Sub category First'
  }
  palceholderSub() {
    return
  }
  Getintouch(myForm) {
 if(this.User != null){
  if (this.rest.networkCheck == true) {
    if (myForm.status === 'INVALID') {
      Object.keys(myForm.controls).forEach(field => {
        const control = myForm.controls[field];
        control.markAsTouched({ onlySelf: true });
      })
    }
    else {

      let loader = this.loadingCtrl.create({
        content: "loading..."
      });
      loader.present();
    
      let postParams = {
        id: this.User._id,
        name: this.User.name,
        email: this.User.email,
        phone: this.User.phone,
        address: this.User.address,
        MainCategoryId: this.AgeGroup,
        SubCategoryId: this.GroupSubject,
        Status: 'Open',
        Message: this.description,
      }
      console.log("postParams>>>", postParams)
      this.rest.Query((postParams))
        .then(data => {
          console.log(data)
          if (data['status']) {
            let alert = this.alertCtrl.create({
              title: 'Message',
              message: data['message'],
              cssClass: 'arrow-account',
              buttons: [
                {
                  text: 'Ok',
                  role: 'cancel',
                  handler: () => {
                    console.log('Cancel clicked');
                    loader.dismiss();
                    this.navCtrl.setRoot(HomePage);
                  }
                }

              ]
            });
            alert.present();

          }
          if (data['status'] == false && data['response_code'] == 'MESSAGE_NOT_SENT') {
            loader.dismiss();
            alert(data['message']);
            const toast = this.toastCtrl.create({
              message: 'Internal Serrver Error',
              duration: 3000
            });
            toast.present();
          }
        }, err => {
          if (err.status == 405 || err.status == 500 || err.status == 0) {
            const toast = this.toastCtrl.create({
              message: 'Internal Serrver Error' + '-' + err['status'],
              duration: 3000
            });
            toast.present();
          }
          loader.dismiss();
          console.log(err);// Error getting the data
        });
    }
  }
  else {
    this.rest.checkNetwork();
  }

 }

 else{
  let alert = this.alertCtrl.create({
    title: 'Message',
    message: 'Please Login to sent a query',
    cssClass: 'arrow-account',
    buttons: [
      {
        text: 'Ok',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');        
          this.navCtrl.setRoot(SignUpPage);
        }
      }

    ]
  });
  alert.present();
 }  


  }

}



