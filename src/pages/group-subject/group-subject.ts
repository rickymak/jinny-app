import { Component, ViewChild } from '@angular/core';
import {
  AlertController, App,
  Content,
  IonicPage,
  LoadingController,
  Modal, ModalController,
  ModalOptions,
  NavController,
  NavParams, Platform,
  ToastController
} from 'ionic-angular';
import { RestProvider } from "../../providers/rest/rest";


@IonicPage()
@Component({
  selector: 'page-group-subject',
  templateUrl: 'group-subject.html',
})
export class GroupSubjectPage {
  @ViewChild(Content) content: Content;
  GetLevelSubCategory: any;
  validation: any;
  GroupTitle: any;
  view: boolean = true;
  notFound: boolean = false;
  condition: any;
  constructor(public toastCtrl: ToastController, public loadingCtrl: LoadingController, public app: App, public platform: Platform, public alert: AlertController, public modalCtrl: ModalController, public rest: RestProvider, public navCtrl: NavController, public navParams: NavParams) {
 this.GroupTitle = localStorage.getItem("gruoptitle")
this.getSubCategory();
  }

  getSubCategory() {
    let loader = this.loadingCtrl.create({
      content: "Loading..."
    });
    loader.present();
    if (this.rest.networkCheck == true) {
      var id = localStorage.getItem("MainCategoryId");
      let postData = {
        postId: id,

      };
      this.rest.subCatById((postData)).then((result) => {
        if (result['status'] == true) {
          loader.dismiss();
          this.GetLevelSubCategory = (result["data"]);

        }
        else if (result['status'] == false) {
          loader.dismiss();
          this.validation = 'NotFound';
        }
      }, (err) => {

        if (err.status == 405 || err.status == 500 || err.status == 0) {
          const toast = this.toastCtrl.create({
            // message: err['status'],
            message: 'Internal Serrver Error' + '-' + err['status'],
            duration: 3000
          });
          toast.present();
          loader.dismiss();
        }
      });
    }
    else {
      this.rest.checkNetwork();
    }
  }

  gotodetail(SubId) {

    localStorage.setItem('SubID', SubId);

    this.navCtrl.push('FindTutorOrStudentPage');
  }
  back()
  {
   this.navCtrl.pop();
  }
}
