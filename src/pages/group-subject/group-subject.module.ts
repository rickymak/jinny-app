import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GroupSubjectPage } from './group-subject';

@NgModule({
  declarations: [
    GroupSubjectPage,
  ],
  imports: [
    IonicPageModule.forChild(GroupSubjectPage),
  ],
})
export class GroupSubjectPageModule {}
