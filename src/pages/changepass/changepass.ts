import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController, ToastController, ViewController, App } from 'ionic-angular';

import { RestProvider } from "../../providers/rest/rest";
import { LoginPage } from "../login/login";

/**
 * Generated class for the ChangepassPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-changepass',
  templateUrl: 'changepass.html',
})
export class ChangepassPage {


  old: any;
  new: any;
  confirmnew: any;
  UserData: any;
  public lang;
  User:any;
  constructor(public rest: RestProvider, public app: App, public navCtrl: NavController, public navParams: NavParams, public viewcontrolller: ViewController, public loadingCtrl: LoadingController, public toastCtrl: ToastController) {
    this.lang = this.rest.Lang;
    this.old = '';
    this.new = '';
    this.confirmnew = '';
    this.User = JSON.parse(localStorage.getItem("User"));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChangepassPage');
  }
  close() {
    this.viewcontrolller.dismiss();
  }
  change() {
    if (this.rest.networkCheck == true) {
      var toaster = this.toastCtrl.create({
        duration: 3000,
        position: 'bottom'
      });
      if (this.old == '' || this.new == '' || this.confirmnew == '') {
        toaster.setMessage('All fields are required ');
        toaster.present();
      }
      else if (this.new.length < 7 && this.confirmnew.length) {
        toaster.setMessage('Password is not strong. Try giving more than six characters');
        toaster.present();
      }
      else if (this.new.length != this.confirmnew.length) {
        toaster.setMessage('New and Confirm Password Does Not Match');
        toaster.present();
      }
      else {
        let loader = this.loadingCtrl.create({
          content: "loading..."
        });
        loader.present();
      
        let postParams = {
          postid:this.User._id,      
          password: this.old,
          NewPassword: this.confirmnew,
        }       
        this.rest.changepassword((postParams)).then((result) => {        
          if (result["status"] == false ) {
            const toast = this.toastCtrl.create({           
              message: result['message'],
              duration: 3000
            });
            toast.present()
            loader.dismiss();
          }
          if (result["status"] == true ) {

            const toast = this.toastCtrl.create({
              message: result['message'],
              duration: 3000
            });
            loader.dismiss();
            toast.present();
            this.clearlocalstorage();
          }
          if (result["status"] == false && result['response_code'] == 'CHANGE_PASSWORD_FAIL') {
            const toast = this.toastCtrl.create({
              message: result['message'],
              duration: 3000
            });
            loader.dismiss();
            toast.present();
          }

        }, err => {
          if (err.status == 405 || err.status == 500 || err.status == 0 || err.status == 404) {
            const toast = this.toastCtrl.create({             
              message: 'Internal Server Error' + '-' + err['status'],
              duration: 3000
            });
            toast.present();
            loader.dismiss();
          }
          console.log(err);// Error getting the data
        });
      }
    }
    else {
      this.rest.checkNetwork();
    }
  }
  clearlocalstorage() {
    localStorage.clear();
    this.app.getRootNav().setRoot(LoginPage);
  }
}

